--SCRIPT PARA CUANDO SE CORRE POR PRIMERA VEZ EL SISTEMA EN ENTORNOS 

--ENTARDA MENU (db)

INSERT INTO public.entrada_menu(
             nombre, menu, url, indice, time_create, usr_create, time_update, usr_update, width, height, fecha_baja, url_mantenimiento)
    VALUES ( 'Mesa de Entrada Previsión', 6, '/MesaEntradaPrevision/', 0, now(), 'postgres', now(), 'postgres', 700, 500, null, null);


--ROLES (db)


INSERT INTO public.rol (nombre, descripcion, usr_create, usr_update, time_create, time_update) values 
('mesa_entrada_prevision', 'Sistema de Mesa de Entrada de Prevision ', 'postgresql', 'postgresql', now(), now()),
( 'mesa_entrada_prevision_admin', 'Sistema de Mesa de Entrada de Prevision - Acceso al Sistema como Admin' , 'postgres', 'postgres', now(), now());




--ENTRADA MENU ROL
INSERT INTO public.entrada_menu_rol(entrada_menu, rol, usr_create, usr_update, time_create, time_update)
	VALUES ((select codigo from public.entrada_menu where nombre = 'Mesa de Entrada Previsión'), (select id from public.rol where nombre = 'mesa_entrada_prevision'), 'postgres', 'postgres', now(), now());


--USUARIOS ROLES



