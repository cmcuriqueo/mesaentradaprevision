/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.issys.mesaentradaprevision.resources;

import com.issys.rest.util.BaseMimeResource;
import javax.ws.rs.core.Response;

/**
 *
 * @author cmcuriqueo
 */
public class AppResource extends BaseMimeResource{
    @Override
    public Response view(String ruta) {
        return super.view("WEB-INF/" + ruta);
    }
}
