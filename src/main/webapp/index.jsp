<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%--
    Document   : media
    Created on : 17 mar. 2020, 08:08:51
    Author     : cmcuriqueo
--%>


<t:block template="/WEB-INF/layouts/layout" title="Ejemplo inclusion de tabs en sus modulos" modulo="Example">
    <jsp:attribute name="layout_tabs" trim="false">
        <jsp:include page="/WEB-INF/layouts/_tabs.jsp"><!-- este ejemplo toma los del menu principal si hay, se puede con cualquier archivo de tabs-->
            <jsp:param name="tab" value="1" />
        </jsp:include>
    </jsp:attribute>
    <jsp:attribute name="content" trim="false">
            
            <!-- jsp con el contenido-->

        <div class="card">
            <div class="card-body">
                Nice!...
            </div>
        </div>

    </jsp:attribute>
</t:block>
