
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:block template="/error/layout_error" title="Error 403" modulo="Mutual">
    <jsp:attribute name="content" trim="false">
        <style type="text/css">
            fieldset legend {color: #086634; padding: 1rem 0px;}
        </style>
        <center>
            <h1 style="    font-size: 4rem; color: #086634;">Error 403</h1>
            <h2 style="margin-top:30px; font-size:2em;">El acceso al recurso pedido ha sido denegado</h2>
            <p>Regresar al <a href="<%=request.getContextPath()%>/">inicio</a></p>
            <div id="msg_error">
            </div>
        </center>
    </jsp:attribute>
</t:block>

