<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:block template="/error/layout_error" title="Error 500" modulo="Mutual">
    <jsp:attribute name="content" trim="false">
        <style type="text/css">
            fieldset legend {color: #086634; padding: 1rem 0px;}
        </style>
        <center>
            <h1 style="    font-size: 4rem; color: #086634;">Error 500</h1>
            <h2 style="margin-top:30px; font-size:2em;">Disculpe, existe un error en la aplicaci&oacute;n. Intentelo mas tarde.</h2>
            <p>Regresar al <a href="<%=request.getContextPath()%>/">inicio</a></p>
            <div id="msg_error">
            </div>
        </center>

    </jsp:attribute>
</t:block>

