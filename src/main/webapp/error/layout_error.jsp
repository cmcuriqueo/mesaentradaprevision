<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:block>
    <jsp:body>
        <!DOCTYPE html>
        <html lang="es">
            <head>
                <meta charset="utf-8"/>
                <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
                <title>${title} - ${modulo} - ${pageContext.servletContext.getServletContextName()} - ISSyS</title>
                <jsp:include page="/WEB-INF/template/_media.jsp"/>
                ${extra_assets}
            </head>
            <style type="text/css">
                img#escudo-chubut {
                    width: 50px;
                    height: 100%;
                }
                img#logo-issys {
                    width: 340px;
                    height: 100%;
                }
                @media (min-width: 576px) { section#body {min-height: 100px} }

                @media (min-width: 768px) { section#body {min-height: 300px} }

                @media (min-width: 992px) { section#body {min-height: 600px} }

                @media (min-width: 1200px) { section#body {min-height: 750px} }

                @media (min-width: 1500px) { section#body {min-height: 800px} }
            </style>
            <script>
                function maximizar() {
                    window.moveTo(0, 0);
                    window.resizeTo(screen.availWidth, screen.availHeight);
                }
            </script>    
            <body onload="maximizar()">
                <!-- Código Encabezado -->
                <section id="body" style="height: 100%;width: 100%;">
                    <div id="linea-top-header" class="container-fluid"></div>
                    <header id="fondo-header" class="container-fluid py-3" style="background-image: url(<c:url value="/resources/fondo-header.png"/>);">
                        <div class="container">
                            <div class="row justify-content-between mt-3">
                                <img id="logo-issys" src="<c:url value="/resources/logo-issys.png"/>" class="pl-3" alt="logo-issys">
                                <img id="escudo-chubut" src="<c:url value="/resources/escudo-chubut.png"/>" class="mb-2" alt="escudo-chubut">
                            </div>
                            <div class="col-md-6 pl-0">
                                <h2 class="mt-4" style="color: #086634;">Importaci&oacute;n de Novedades</h2>
                            </div>
                            <hr class="mt-1">
                        </div>
                    </header>
                    <div style="padding-top: 20px;" class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                ${content}
                            </div>
                        </div>


                    </div>
                </section>
                <section id="footer" style="position: absolute;width: 100%;">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-secondary" style="text-align: left">
                                    <i class="fa fa-info-circle fa-lg"></i> <strong style="">Manual de usuario "Importaci&oacute;n de Novedades" <a target="_blank" rel="noopener noreferrer" href="https://docs.google.com/document/d/1dpGSEC93ycmez7sWba1Fh2B1h9PgWxCSQWjk_41C8HQ/edit?usp=sharing">Click Aquí</a>.</strong>
                                    &nbsp;&nbsp;&nbsp;                                      
                                </div>
                            </div>    
                        </div>
                        <div class="row">

                            <div class="col-md-12">
                                <hr>

                                <p class="text-muted">
                                    <img src="<c:url value="/resources/logo-issys.png"/>" width="60px">
                                    Desarrollado por la <code>Dirección de Sistemas de Información</code> (Casa Central) &nbsp;&nbsp;<a onclick="location.reload();" class="btn btn-default btn-xs"><i class="fa fa-refresh"></i></a> 
                                </p>
                            </div>
                        </div>
                    </div>


                </section>
                <script>
                    $(function () {
                        $('[data-toggle="tooltip"]').tooltip();  //tooltips de bs3

                        $(document).ajaxError(function (event, xhr, ajaxOptions, thrownError) {

                            bootbox.hideAll();
                            if (thrownError === "abort")
                                return;

                            if (xhr.status === 400)
                                return;
                            if (xhr.status === 403) {
                                bootbox.dialog({closeButton: false, title: "Acceso denegado", message: "Su sesi&oacute;n ha caducado, o no tiene los permisos suficientes para completar esta acci&oacute;n.", buttons: {"OK": {className: 'btn-danger'}}});
                                return;
                            }
                            if (xhr.status === 500) {
                                bootbox.dialog({closeButton: false, title: "Error del servidor", message: "Ha ocurrido un error. Por favor, contacte al &aacute;rea t&eacute;cnica.", buttons: {"OK": {className: 'btn-danger'}}});
                                return;
                            }
                        });

                    <c:if test="${not empty mensaje_error}">
                        bootbox.dialog({closeButton: false, title: "Operación incompleta - Errores", message: "${mensaje_error}", buttons: {"OK": {className: 'btn-danger'}}});
                    </c:if>
                    <c:if test="${not empty mensaje}">
                        bootbox.dialog({closeButton: false, title: "Operación completada", message: "${mensaje}", buttons: {"OK": {className: 'btn-success'}}});
                    </c:if>

                    });
                </script>
            </body>
        </html>
    </jsp:body>
</t:block>
