<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
    Document   : media
    Created on : 17 mar. 2020, 08:08:51
    Author     : cmcuriqueo
--%>


<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>${titulo} - ${modulo} - ISSyS</title>
    <jsp:include page="_head.jsp"/>
</head>
<body onload="maximizar()">
    <header>
        <jsp:include page="_header.jsp"/>
    </header>
    <main>
        <div style="padding-top: 20px;" class="container-fluid">
            <div class="row">
                <div class="col-md-2">
                    ${layout_tabs}
                </div>
                <div class="col-md-10">
                    ${content}
                </div>
            </div>
        </div>
    </main>
    <footer>
        <jsp:include page="_footer.jsp"/>
    </footer>
    <jsp:include page="_toast.jsp"/><!-- notificaciones copadas -->

    <script>
        $(function () {
            <c:if test="${not empty mensaje_error}">
                bootbox.dialog({closeButton: false, title: "Operación incompleta - Errores", message: "${mensaje_error}", buttons: {"OK": {className: 'btn-danger'}}});
            </c:if>
            <c:if test="${not empty mensaje}">
                bootbox.dialog({closeButton: false, title: "Operación completada", message: "${mensaje}", buttons: {"OK": {className: 'btn-success'}}});
            </c:if>
        });
    </script>
</body>
</html>