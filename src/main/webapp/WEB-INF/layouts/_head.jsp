<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<%--
    Document   : media
    Created on : 17 mar. 2020, 08:08:51
    Author     : cmcuriqueo
--%>

<link type="text/css" href="<%=request.getContextPath()%>/media/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link type="text/css" href="<%=request.getContextPath()%>/media/bower_components/mi_estilo.css" rel="Stylesheet" />
<link type="text/css" href="<%=request.getContextPath()%>/media/bower_components/components-font-awesome/css/fontawesome.min.css" rel="stylesheet" />
<link type="text/css" href="<%=request.getContextPath()%>/media/bower_components/components-font-awesome/css/brands.css" rel="stylesheet">
<link type="text/css" href="<%=request.getContextPath()%>/media/bower_components/components-font-awesome/css/solid.css" rel="stylesheet">
<link type="text/css" href="<%=request.getContextPath()%>/media/bower_components/jquery-ui/themes/base/jquery-ui.css" rel="stylesheet">

<script type="text/javascript" src="<%=request.getContextPath()%>/media/bower_components/jquery/dist/jquery.min.js" rel="stylesheet"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/media/bower_components/bootstrap/dist/js/bootstrap.min.js" rel="stylesheet"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/media/bower_components/jquery-ui/jquery-ui.min.js" rel="stylesheet"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/media/bower_components/bootboxjs/bootbox.all.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/media/bower_components/bootboxjs/bootbox.locales.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/media/bower_components/mi_script.js"></script>
