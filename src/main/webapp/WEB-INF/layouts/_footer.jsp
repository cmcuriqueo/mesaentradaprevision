<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%--
    Document   : media
    Created on : 17 mar. 2020, 08:08:51
    Author     : cmcuriqueo
--%>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <hr>
            <p class="text-muted">
                <img src="<c:url value="/media/image/logo_issys_chico.jpg"/>" width="60px"/>
                Desarrollado por la <code>Direcci&oacute;n de Sistemas de Informaci&oacute;n</code> (Casa Central) &nbsp;&nbsp;<a onclick="location.reload();" class="btn btn-default btn-xs"><i class="fa fa-refresh"></i></a> 
            </p>
        </div>
    </div>
</div>