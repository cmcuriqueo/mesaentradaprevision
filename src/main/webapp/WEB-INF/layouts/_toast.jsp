<!-- componente de notificaciones con el logo de issys copado-->
<%--
    Document   : media
    Created on : 17 mar. 2020, 08:08:51
    Author     : cmcuriqueo
--%>

<div class="position-absolute w-100 d-flex flex-column p-4">
    <div class="toast ml-auto" role="alert" data-delay="700" data-autohide="false">
        <div class="toast-header">
            <img src="https://extranet.issys.gov.ar/favicon.ico"/>&nbsp;
            <strong class="mr-auto" id="title_flash">{{ <c:out value="${title_flash}"/> }}</strong>
            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="toast-body" id="toast-body"> {{ <c:out value="${body_flash}"/> }} </div>
    </div>
</div>


<c:if test="${flash_msj}">
<script>

    $(function () {
        $('.toast').toast('show');
    });
</script>
</c:if>


