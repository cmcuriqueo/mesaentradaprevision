<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<div style="background: #294563;" >
    <jsp:include page="_header_intranet.jsp">
        <jsp:param name="titulo" value="${pageContext.servletContext.getServletContextName()}" />
        <jsp:param name="subtitulo" value="${modulo}" />
    </jsp:include>
</div>