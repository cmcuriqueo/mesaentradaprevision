<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%--
    Document   : media
    Created on : 17 mar. 2020, 08:08:51
    Author     : cmcuriqueo
--%>

<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical" style="        font-size: 11px;">
    <c:forEach var="tab_item" items="${tabs}">
        <c:if test="${param.tab eq tab_item[1]}">
            <c:set var="a_class" value="nav-link active"/>
        </c:if>
        <c:if test="${param.tab ne tab_item[1]}">
            <c:set var="a_class" value="nav-link"/>
        </c:if>
        <a href="<c:url value="${tab_item[2]}"/>" class="${a_class}"><strong>${tab_item[0]}</strong></a>
    </c:forEach>

</div>
<ul class="nav nav-pills nav-stacked" style="border-top: 1px dashed #ccc"></ul>