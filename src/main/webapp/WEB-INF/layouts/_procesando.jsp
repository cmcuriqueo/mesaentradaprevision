<%--
    Document   : media
    Created on : 17 mar. 2020, 08:08:51
    Author     : cmcuriqueo
--%>


<div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modalProcesando"> 
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <div class="col-md-12"><i class="fas fa-spinner fa-pulse fa-9x" style="color:#1f4e83"></i></div>
                <div class="col-md-12"><h4><b>Procesando... Por favor espere.</b></h4></div>
            </div>
        </div>
    </div>
</div>