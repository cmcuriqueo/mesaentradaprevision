function maximizar(){
    window.moveTo(0, 0);
    window.resizeTo(screen.availWidth, screen.availHeight);
}

$(function () {
    $('[data-toggle="tooltip"]').tooltip();  //tooltips de bs3

    $('#v-pills-tab a').click(function(e){
        window.location.href = $(this).attr("href");
        return false;
    });
    $('.menu .list-group-item').on('click', function () {
        $('.fa', this)
                .toggleClass('fa-rotate-90');
    });

    $(document).ajaxError(function (event, xhr, ajaxOptions, thrownError) {

        
        bootbox.hideAll();

        if (thrownError === "abort")
            return;

        if (xhr.status === 400)
            return;
        if (xhr.status === 403) {
            bootbox.dialog({closeButton: false, title: "Acceso denegado", message: "Su sesi&oacute;n ha caducado, o no tiene los permisos suficientes para completar esta acci&oacute;n.", buttons: {"OK": {className: 'btn-danger'}}});
            return;
        }
       
        bootbox.dialog({closeButton: false, title: "Error del servidor", message: "Ha ocurrido un error. Por favor, contacte al &aacute;rea t&eacute;cnica.", buttons: {"OK": {className: 'btn-danger'}}});
    });
});