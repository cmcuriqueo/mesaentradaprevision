<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@tag description="Comprueba el nombre del rol con un prefijo por proyecto" pageEncoding="UTF-8"%>
<%@attribute name="rol"%>
<c:if test="${pageContext.request.isUserInRole(prefijo_rol.concat(rol))}">
    <jsp:doBody/>
</c:if>
